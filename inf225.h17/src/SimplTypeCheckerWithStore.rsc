module SimplTypeCheckerWithStore
import ParseTree;
import IO;
import String;
import Simpl;
import SimplAST;

alias TCSEnv = map[str, tuple[TypeAST, int]];


TypeAST typecheck((Type)`int`) = SimplAST::Int();

TypeAST typecheck((Type)`<Type t1> -\> <Type t2>`)
	= Fun([typecheck(t1)], typecheck(t2));

default TypeAST typecheck(Type t) { 
	throw "Unknown type <t>, <t@\loc>";
}

tuple[ExprAST,TypeAST] typecheck((Expr)`(<Expr e>)`, TCSEnv env) {
	return typecheck(e, env);
}

tuple[ExprAST,TypeAST] typecheckOperator(str opName, Expr e1, Expr e2, TCSEnv env) {
	<a1, t1> = typecheck(e1, env);
	<a2, t2> = typecheck(e2, env);
	if(t1 == Int() && t2 == Int())
		return <Apply(Builtin("int::<opName>"), [a1, a2]), Int()>;
	else if(t1 == String() && t2 == String())
		return <Apply(Builtin("string::append"),[a1, a2]), String()>;
	else {
	// Eventuelt:
	//    * throw feil
	//    * skriv ut feilmelding
	//    * returner feilmeldinger i en egen liste
		return Error("* expected int arguments", e@\loc) ;
	}
}

tuple[ExprAST,TypeAST] typecheck(e : (Expr)`<Expr e1> + <Expr e2>`, TCSEnv env) {
	return typecheckOperator("+", e1, e2, env);
}

tuple[ExprAST,TypeAST] typecheck(e : (Expr)`<Expr e1> * <Expr e2>`, TCSEnv env)
	= typecheckOperator("*", e1, e2, env);

tuple[ExprAST,TypeAST] typecheck(e : (Expr)`<Expr e1> - <Expr e2>`, TCSEnv env)
	= typecheckOperator("-", e1, e2, env);

tuple[ExprAST,TypeAST] typecheck(e : (Expr)`<Expr e1> \< <Expr e2>`, TCSEnv env)
	= typecheckOperator("\<", e1, e2, env);

tuple[ExprAST,TypeAST] typecheck(fullE : (Expr)`if <Expr c> then <Expr t> else <Expr e> end`, TCSEnv env) {
	<aC, tC> = typecheck(c, env);
	<a1,t1> = typecheck(t, env);
	<a2,t2> = typecheck(e, env);
	newNode = If(aC, a1, a2);
	
	if(tC == Int()) {
		if(t1 == t2) {
			return <newNode, t1>;
		}
		else {
			return <Error("Branches should have same type: <t1> != <t2>", fullE@\loc, newNode), t1>;
		}
	}
	else {
		return <Error("Condition should return int", c@\loc, newNode), t1>;
	}
}

tuple[ExprAST,TypeAST] typecheck((Expr)`let <Var v> = <Expr e1> in <Expr e2> end`, TCSEnv env) {
//	TCSEnv localTCSEnv = env; // begge to refererer til samme verdi
//	localTCSEnv["<v>"] = typecheck(e1, env); // localTCSEnv refererer til en ny map, med den ekstra bindingen – env er uendret
//	return typecheck(e2, localTCSEnv);
	// dette funker like grei; endringen av "env" har ingen effekt utenfor
	// denne funksjonen
	
	<a1, t1> = typecheck(e1, env);
	<_,l> = env["__next_loc"];
	env["<v>"] = <t1, l>;
	env["__next_loc"] = <Int(), l+1>;
	<a2, t2> = typecheck(e2, env);
	
	//if("<v>" in env) {
	//	return <Error("Redefined variable <v>", v@\loc, Let("<v>", a1, a2)), t2>;
	//}
	//else {
		return <Let("<v>", a1, a2), t2>;
	//}
}

Env loadDecl((Def)`<TypeAST rt> <Var f>(<TypeAST t> <Var a>) = <Expr e1>;`, TCSEnv env) {
	argType = typecheck(t);
	retType = typecheck(rt);

	<_, l> = env["__next_global_loc"];
	env["__next_global_loc"] = <Int(), l+1>;
	env = env + ("<f>" : <Fun([argType], retType), Global(l)>);
	return env;
}

DefAST typecheckDef((Def)`<TypeAST rt> <Var f>(<TypeAST t> <Var a>) = <Expr e1>;`, TCSEnv env) {
	// let twice(x) = x * x in twice(2) end
	bodyEnv = env + ("__next_loc" : <Int(), Local(0)>,
					"<a>" : <argType, Local(-1)>,
					"<f>" : <Fun([argType], retType), Local(-2)>);
	<bodyAST, bodyType> = typecheck(e1, bodyEnv);
	maxLoc = max([l | /Var(_, Local(l)) <- bodyAST]);
	
	
	if(retType == bodyType) {
		return <FunDef("<f>", [Param(argType, "<a>")], bodyAST, maxLoc), env>;
	}
	else {
		return <Error("Expected return type <retType>, got <bodyType>", rt@\loc, FunDef("<f>", [Param(argType, "<a>")], bodyAST)), retType>;
	}
}

tuple[ExprAST,TypeAST] typecheck((Expr)`<Expr f>(<Expr arg>)`, TCSEnv env) {
	<funAST, funTypeAST> = typecheck(f, env);
	<inputAST, inputTypeAST> = typecheck(arg, env);
	
	if(Fun([argTypeAST], retTypeAST) := funTypeAST) {
		if(argTypeAST == inputTypeAST) {
			return <Apply(funAST, [inputAST]), retTypeAST>;
		}
		else {
			return <Error("Wrong argument type, expected <argTypeAST>", arg@\loc, Apply(funAST, [inputAST])), retTypeAST>;
		}
	}
	else {
		return <Error("Not a function: <f>", f@\loc, Apply(funAST, [inputAST])), funTypeAST>;
	}
}



tuple[ExprAST,TypeAST] typecheck((Expr)`<Num a>`, TCSEnv env) = <Int(toInt("<a>")), Int()>;


tuple[ExprAST,TypeAST] typecheck((Expr)`<Expr e1> ; <Expr e2>`, TCSEnv env) {
	<a1, t1> = typecheck(e1, env);
	<a2, t2> = typecheck(e2, env);
	
	return <Seq(a1, a2), t2>;
}

tuple[ExprAST,TypeAST] typecheck((Expr)`<Var a>`, TCSEnv env) {
	v = "<a>";
	if(v in env) {
		<t,l> = env[v];
		return <Var("<a>", l), t>;
	}
	else {
		return <Error("Undefined variable <v>", a@\loc, Var("<a>")), Int()>;
	}	
}

default tuple[ExprAST,TypeAST] typecheck(Expr e, TCSEnv env) {
	throw "Unknown expression <e>";
}

tuple[ExprAST,TypeAST] typecheck(amb(alternatives), TCSEnv env) {
	TypeAST s = Int();
	for(alt <- alternatives) {
		s = s + "\n====\n<typecheck(alt, env)>\n====\n";
	}
	return Error("Ambiguity", |unknown://|);
}
