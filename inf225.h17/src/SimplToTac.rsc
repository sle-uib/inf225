module SimplToTac
import ParseTree;
import IO;
import String;
import Simpl;

alias CompileEnv = map[str,str] ;


public str simplToTac((Stat)`<Var v> = <Expr e>;`, CompileEnv env) {
	n = "<v>";
	return simplToTac(e, n, env);
}

public str simplToTac(list[Stat] ss, CompileEnv env) {
	return intercalate("", [simplToTac(s, env) | s <- ss]); 
}

public str simplToTac((Stat)`if <Expr e> then <Stat* ss1> else <Stat* ss2> end`, CompileEnv env) {
	t0 = newTmp();
	ic = simplToTac(e, t0, env);
	
	iThen = simplToTac([s | s <- ss1], env);
	iElse = simplToTac([s | s <- ss2], env);
	
	thenLabel = newTmp("then");
	elseLabel = newTmp("else");
	endLabel = newTmp("end");
	
	return "<ic>\n"
		+  "\tif <t0> then goto <thenLabel>\n"
		+	"<elseLabel>:\t<iElse>"
		+	"\tgoto <endLabel>\n"
		+	"<thenLabel>:\t<iThen>"
		+	"<endLabel>:\n"
		;
}

public str simplToTac((Stat)`while <Expr e> do <Stat* ss> end`, CompileEnv env) {
	t0 = newTmp();
	ic = simplToTac(e, t0, env);
	
	iBody = simplToTac([s | s <- ss], env);

	startLabel = newTmp("start");
	endLabel = newTmp("end");

	return "<startLabel>: <ic>"
	    + "\tif !<t0> then goto <endLabel>\n"
		+ "<iBody>"
		+ "\tgoto <startLabel>\n"
		+ "<endLabel>:\n"
		;
}
public str simplToTac((Expr)`<Expr e1> + <Expr e2>`, str n, CompileEnv env) {
	t0 = newTmp();
	t1 = newTmp();
	i1 = simplToTac(e1, t0, env);
	i2 = simplToTac(e2, t1, env);

	return  "push <t0>\n"
		+   "push <t1>\n"
		+   "add\n"
		+   "pop <n>\n"
		; 
	
	//"<i1><i2>\t<n> = <t0> + <t1>;\n";
}

public str simplToTac((Expr)`<Num e>`, str n, CompileEnv env) {
	return "\t<n> = <unparse(e)>;\n";
}

public str simplToTac((Expr)`<Var e>`, str n, CompileEnv env) {
	v = "<unparse(e)>";
	if(v == n)
		return "";
	else
		return "\t<n> = <v>;\n";
}

int i = 0;
public str newTmp() = newTmp("t");

public str newTmp(str s) {
	n = "<s><i>";
	i = i + 1;
	return n;
}

/*


public Expr simplToTac(Expr e) {
	visit(e) {
		case (Expr)`<Expr e1> + <Expr e2>` => (Expr)`add(<Expr e1>, <Expr e2>)`
	}	
}

public Expr simplToTac((Expr)`<Var v> = <Expr e>`) {
	<v1, s1> = decompose(e1);
	<v2, s2> = decompose(e2);
	
	
	while(hasOnlyPrimitiveArgs(e)) {
		if(/(Expr)`<Expr subExpr>` := e) {
			if(subExpr != e) {
				  (Expr)`<Var tmp> = <Expr subExpr>`;
			}
		}
	}
}

public bool hasOnlyPrimitiveArgs((Expr)`<Expr e>(<{Expr ","}* args>`)  {
	for(x <- [e, *[a | a <- args]]) {
		if(!((Expr)`<Var _>` := x  || (Expr)`<Num _>` := x)) {
			return false;
		}
	}
	return true;
}
public Expr simplToTac((Expr)`<Expr e1> + <Expr e2>`) {
	<v1, s1> = decompose(e1);
	<v2, s2> = decompose(e2);
	
	return (Expr)`<Expr s1>; <Expr s2>; <Expr v1> + <Expr v2>`;
}

public Expr simplToTac((Expr)`<Var v> = <Expr e1> + <Expr e2>`) {
	<v1, s1> = decompose(e1);
	<v2, s2> = decompose(e2);
	
	return (Expr)`<Expr s1>; <Expr s2>; <Var v> = (<Expr v1> + <Expr v2>)`;
}



public tuple[Expr,Expr] decompose(e:(Expr)`<Var v>`) = <e, (Expr)`0`>;
public tuple[Expr,Expr] decompose(e:(Expr)`<Num n>`) = <e, (Expr)`0`>;
public tuple[Expr,Expr] decompose(Expr e) {
	name = "t<i>";
	i = i + 1;
	v = parse(#Var, name);
	println(e);
	
	compute = simplToTac((Expr)`<Var v> = <Expr e>`);

 	return <(Expr)`<Var v>`, compute>;
}

*/