module Plugin

import util::IDE;
import ParseTree;
import IO;
import Simpl;
//import SimplTypeChecker;
import SimplAST;
import imperative::Simpr;
import imperative::SimprAST;
import imperative::SimprTypeChecker;
import Message;

void main() {
   registerLanguage("Simpl", "simpl", Tree(str src, loc l) {
     pt = parse(#start[Expr], src, l);
  //   <ast, _> = typecheck(pt, ());
     pt = visit(pt) {
     	case (Type)`<Type t>` => t[@category="Constant"]
     };
     
     return pt;
   });
   
   registerContributions("Simpl", {
   		builder(set[Message] (Tree t) {
   			println("build!");
 			ast = typecheck(t, ());
 			return {error(s, l) | /Error(s, l) <- ast};
   		})
   });


   registerLanguage("Simpr", "simpr", Tree(str src, loc l) {
     pt = parse(#start[Program], src, l);
  //   <ast, _> = typecheck(pt, ());
     pt = visit(pt) {
     	case (Type)`<Type t>` => t[@category="Constant"]
     };
     
     return pt;
   });

   registerContributions("Simpr", {
   		builder(set[Message] (Tree t) {
   			if(Program p := t.top) {
   				//println("build!");
 				<ast,errs> = typecheck(p);
	 			return errs;
 			}
   		})
   });

}