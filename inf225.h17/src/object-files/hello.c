#include <stdio.h>

int main(int argc, char** argv) {
	puts("Hello, world!\n");
	int x = 42;
	int y = x * 17;

	double a = 42.0;
	double b = a * 17.0;

	unsigned char m = 42;
	unsigned short n = m * 17;

	printf("%d %f %d\n", y, b, n);
	return argc;
}
