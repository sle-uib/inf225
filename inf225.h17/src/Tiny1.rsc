module Tiny1
import ParseTree;
import IO;
import String;


syntax Expr 
	= "(" Expr ")"
	| Expr "+" Expr
	| Expr "*" Expr
	| Var
	| Num
	;

lexical Var = [a-zA-Z]+;

lexical Num = [0-9]+;

layout LAYOUT = [\ \n\r\f\t]*;
