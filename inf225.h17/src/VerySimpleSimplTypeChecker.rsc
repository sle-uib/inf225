module VerySimpleSimplTypeChecker
import ParseTree;
import IO;
import String;
import Simpl;

data Type
	= Fun(Type arg, Type retType)
	| Int()
	| Error(str msg, loc location)
	;

alias TCEnv = map[str, Type];

Type verySimpleTC((Type)`int`) = Int();

Type verySimpleTC((Type)`<Type t1> -\> <Type t2>`)
	= Fun(verySimpleTC(t1), verySimpleTC(t2));

default Type verySimpleTC(Type t) = Error("Unknown type <t>", t@\loc);


Type verySimpleTC(amb(alternatives), TCEnv env) {
	Type s = Int();
	for(alt <- alternatives) {
		s = s + "\n====\n<verySimpleTC(alt, env)>\n====\n";
	}
	return Error("Ambiguity", |unknown://|);
}


Type verySimpleTC((Expr)`(<Expr e>)`, TCEnv env) {
	return verySimpleTC(e, env);
}

Type verySimpleTC((Expr)`<Expr e1> * <Expr e2>`, TCEnv env) {
	t1 = verySimpleTC(e1, env);
	t2 = verySimpleTC(e2, env);
	if(t1 == Int() && t2 == Int())
		return Int();
	else
		throw "* expected int arguments";
}

Type verySimpleTC((Expr)`<Expr e1>+<Expr e2>`, TCEnv env) {
	if(<Int(), Int()> := <verySimpleTC(e1, env), verySimpleTC(e1, env)>)
		return Int();
	else
		throw "+ expected int arguments";
}


Type verySimpleTC(e : (Expr)`<Expr e1>-<Expr e2>`, TCEnv env) =
	<Int(), Int()> := <verySimpleTC(e1, env), verySimpleTC(e1, env)>
	? Int()
	: Error("+ expected int arguments", e@\loc);


Type verySimpleTC(e : (Expr)`<Expr e1> \< <Expr e2>`, TCEnv env) =
	<Int(), Int()> := <verySimpleTC(e1, env), verySimpleTC(e1, env)>
	? Int()
	: Error("+ expected int arguments", e@\loc);

Type verySimpleTC(fullE : (Expr)`if <Expr c> then <Expr t> else <Expr e> end`, TCEnv env) {
	if(verySimpleTC(c, env) != Int()) {
		return Error("Condition should return int", c@\loc);
	}
	t1 = verySimpleTC(t, env);
	t2 = verySimpleTC(e, env);
	if(t1 == t2) {
		return t1;
	}
	else {
		return Error("Branches should have same type", fullE@\loc);
	}
}


Type verySimpleTC((Expr)`let <Var v> = <Expr e1> in <Expr e2> end`, TCEnv env) {
//	TCEnv localTCEnv = env; // begge to refererer til samme verdi
//	localTCEnv["<v>"] = verySimpleTC(e1, env); // localTCEnv refererer til en ny map, med den ekstra bindingen – env er uendret
//	return verySimpleTC(e2, localTCEnv);
	// dette funker like grei; endringen av "env" har ingen effekt utenfor
	// denne funksjonen
	env["<v>"] = verySimpleTC(e1, env);
	return verySimpleTC(e2, env);
}

Type verySimpleTC((Expr)`let <Var f>(<Type t> <Var a>) = <Expr e1> in <Expr e2> end`, TCEnv env) {
	// let twice(x) = x * x in twice(2) end
	argType = verySimpleTC(t);
	bodyEnv = env + ("<a>" : argType);
	bodyType = verySimpleTC(e1, bodyEnv);

	env["<f>"] = Fun(argType, bodyType);
	return verySimpleTC(e2, env);
}

Type verySimpleTC((Expr)`<Expr f>(<Expr arg>)`, TCEnv env) {
	funType = verySimpleTC(f, env);
	inputType = verySimpleTC(arg, env);
	
	if(Fun(argType, retType) := funType) {
		if(argType == inputType) {
			return retType;
		}
		else {
			return Error("Wrong argument type, expected <argType>", arg@\loc);
		}
	}
	else {
		return Error("Not a function: <f>", f@\loc);
	}
}



Type verySimpleTC((Expr)`<Num a>`, TCEnv env) = Int();

Type verySimpleTC((Expr)`<Var a>`, TCEnv env) {
	v = "<a>";
	if(v in env) {
		return env[v];
	}
	else
		return Error("Undefined variable <v>", a@\loc);	
}

default Type verySimpleTC(Expr e, TCEnv env) {
	throw "Unknown expression <e>";
}
