import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ClosureExample {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>(Arrays.asList("mango", "banan", "eple", "appelsin"));


		list = list.stream().map(makeFun()).collect(Collectors.toList());

		System.out.println(list);
	}
	
	static Function<String, String> makeFun() {
		String badFruit = "appelsin";
		Function<String,String> fun = new Function<String, String>() {

			@Override
			public String apply(String arg0) {
				if (arg0.equals(badFruit))
					return "sitron";
				else
					return arg0;
			}
		};
		
		// this isn't allowed in Java, because Java "fakes" closures
		// by bundling the *value* of the variable into the code rather
		// than the variable itself (so it can't be updated anymore)
		
		// badFruit = "eple";
		
		return fun;
	}
}

class MyAnonClass implements Function<String, String> {

	@Override
	public String apply(String t) {
		// TODO Auto-generated method stub
		return null;
	}

}