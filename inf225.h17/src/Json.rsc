module Json

import IO;
import String;


syntax JsonValue
	= "false"
	| "null"
	| "true"
	| JsonObject obj  // we can add labels to symbols if we want to – these can be used to
	                  // access the children of a parse tree node:
	                  //   (JsonValue)`[1,2,3]`.arr
	| JsonArray arr 
	| JsonNumber nbr
	| JsonString strng
	;

syntax JsonArray
	= "[" {JsonValue ","}* "]"
	;
syntax JsonObject
	= "{" {JsonMember ","}* members "}"  // comma-separated list
	
// alternatives:
//	= "{" (JsonMember ",")* "}"  // list with comma at end of each element
//	= "{" (JsonMember ("," JsonMember)*)? "}" // from the standard
	;
	
syntax JsonMember
	= JsonString ":" JsonValue
	;
	

lexical JsonNumber
// BUG: we accept 12345. as a number, but we should have at least one digit after the point
	= [\-]? ([0] | ([1-9] [0-9]*)) ([.] [0-9]*)? ([eE] [\- +]? [0-9]+)?
// in general, it's a good idea to force greedy matching with precede/follow restrictions: 
//	= [0-9] !<< [0-9]+ !>> [0-9]  
	;
	
lexical JsonString
	= [\"] JsonChar* [\"]
	;


// we could inline this above, but it makes sense to have a separate production
lexical JsonChar
	//= [\u0020-\U10FFFF] - [\"\\]
	= ![\a00-\a19 \" \\]
	//![\"\\]    // this is equivalent to [^\"\\] – in Rascal, character classes in 
	//             // grammars are inverted by ! in front
	| [\\] [\" \\ / b f n r t]
	| [\\] [u] [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F]
	;
// fyi – character classes [...] may contain characters or character ranges; some
// characters, like backslash, brackets, hyphen and space must be escaped. Spaces are *not*
// significant, so [0-9 a-f A-F] could be used instead above. There are also operators to combine
// character classes (you've seen ! for negation), including -, || and && (for difference,
// union and intersection). So, ![ \" \\ ] could also be written as one of:
//    ![] - [\"\\]    // class of all chars, minus class of " and \ 
//    [\u0000-\u10FFFF] - [\"\\]    // range of all chars, minus class of " and \
//
// If you look at the definition in the RFC, it says:
//    unescaped = %x20-21 / %x23-5B / %x5D-10FFFF
// so the first 32 characters are actually not valid. So, we should probably have defined
// string characters as:
//    [\u0020-\u10FFFF] - [\"\\]   // == [\u0020-\u0021 \u0023-\u005B \u005D-\u10FFFF]
// 
// more fyi – in general, there is no grammatical difference between a one-character literal
// and a character class with only one alternative, so "0" is the same as [0], and
// "else" is the same as [e] [l] [s] [e]. In practice, however, the parse trees will be
// slightly different, and some parts of Rascal (and perhaps your own code) will interpret
// the parse trees in a different way:
//    * literals typically contribute to the structure, but don't add significant information
//      on their own (e.g., once we've parsed an if-statement, we know that the "if" keyword
//      should be there, and we don't need to store it if we're making an abstract syntax
//      tree, for instance 
//    * character class is typically used where there is a selection of possibilities, and so
//      the assumption is that there *is* significant information; we'll want to store which
//      of the characters were actually found (so, we would normally put it into the AST)
//    * when making IDEs (including the one you're using now), Rascal makes the assumption that
//      a literal with "identifier characters" (e.g., letters, numbers, underscore and hyphen)
//      are keywords in your language, and should be colored / bolded as such. (see example
//      below the grammar – there you can see that "0" and "-" have been defined as literals
//      in Rascal's grammar (e.g., with "0" and "-" instead of [0] and [\-]). There is no
//      syntactic difference, only difference is in how the parse tree is interpreted.
	
layout LAYOUT = [\ \t\n\r]*;


// check the highlighting here, and you'll see - and 0 are bold:
// int a = b - c + d + 0 + 1 + 2;

// str num o value 

public value jsonToValue((JsonValue)`false`) {
	return false;
}
public value jsonToValue((JsonValue)`true`) {
	return true;
}
public value jsonToValue((JsonValue)`null`) {
	return false;
}

public value jsonToValue((JsonValue)`{ <{JsonMember ","}* members> }`) {
	map[str, value] myMap = ();
	
	for((JsonMember)`<JsonString name> : <JsonValue val>` <- members) {
		myMap[jsonToValue(name)] = jsonToValue(val);
	}
	
	return myMap;
}

public value jsonToValue((JsonValue)`<JsonNumber n>`) {
	return toReal("<n>");
}

public str jsonToValue(JsonString s) {
	if(/^\"<t:.*>\"$/ := unparse(s)) {
		return visit(t) { // [\" \\ / b f n r t]
		case /\\n/ => "\n"
		case /\\\\/ => "\\"
		case /\\\"/ => "\""
		case /\\b/ => "\b"
		case /\\f/ => "\f"
		case /\\r/ => "\r"
		case /\\t/ => "\t"
		}
	}
}

public value jsonToValue((JsonValue)`<JsonString s>`) {
	return jsonToValue(s);
}

public value jsonToValue((JsonValue)`[ <{JsonValue ","}* elements> ]`) {
	list[value] myList = [];
	
	for((JsonValue)`<JsonValue val>` <- elements) {
		myList += jsonToValue(val);
	}
	
	return myList;
}

public default value jsonToValue(JsonValue val) {
   throw "Unexpected parse tree node: <unparse(val)>";
}


public default str valueToJson(value val) {
   throw "Unexpected parse tree node: <unparse(val)>";
}

//public str valueToJson([*prefix, "a", *suffix]) {
public str valueToJson(list[value] val) {
	/*
	str s = "";
	boolean first = true;
	for(v <- val) {
		if(!first)
			s = s + ", ";
		else
			first = false;
		s = s + valueToJson(v);
	}
	*/
	
   	return "[<intercalate(", ", [ valueToJson(v) | v <- val ])>]";
}

public str valueToJson(map[str,value] val) {
   	return "{<intercalate(", ", [ "<valueToJson(v)> : <valueToJson(val[v])>"  | v <- val ])>}";
}

public str valueToJson(str val) {
	/* val = top-down visit(val) { // [\" \\ / b f n r t]
		// case /\\/ => "\\\\"
		case /\n/ => "\\n"
		case /\"/ => "\\\""
		case /\b/ => "\\b"
		case /\f/ => "\\f"
		case /\r/ => "\\r"
		case /\t/ => "\\t"
		};
		
		*/
	val = replaceAll(val, "\\", "\\\\");
	val = replaceAll(val, "\n", "\\n");
	val = replaceAll(val, "\r", "\\r");
	val = replaceAll(val, "\b", "\\b");
	val = replaceAll(val, "\t", "\\t");
	val = replaceAll(val, "\f", "\\f");
	val = replaceAll(val, "\"", "\\\"");
		

   return "\"<val>\"";
}

public str valueToJson(num val) {
   return "<val>";
}

