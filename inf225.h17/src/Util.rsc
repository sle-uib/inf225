module Util
import ParseTree;
import IO;
import Set;
import String;
import Node;

public str applToStr(a:appl(p,_)) = "<prodToStr(p)> := \"<replaceAll(unparse(a),"\n","\\n")[..20]>\"";
public str prodToStr(prod(s,as,_)) = "<printSymbol(s, true)> = <intercalate(", ", [printSymbol(a, true) | a <- as])>;";
public str prodToStr(regular(s)) = "<printSymbol(s, true)>";

public void showAmb(Tree t) {
	top-down-break visit(t) {
		case x:amb(as): {
			println("Ambiguity: ");
			as = visit(as) { case amb(ts) => getOneFrom(ts) }
			for(a <- delAnnotationsRec(as)) {
				println(applToStr(a));
				for(s <- a.args) {
					println("\t<applToStr(s)>");
				}
				//println("    <[a]>");
			}
		}
	}
}
