module Tiny
import ParseTree;
import IO;
import String;



//start syntax Expr = "a"; 
start syntax Expr 
	= left Expr Expr
	| "(" Expr ")"
//	> "-" Expr
	> left Expr "*" Expr
	> left (Expr "+" Expr
	     |  Expr "-" Expr)
	> "if" Expr "then" Expr "else" Expr "end"
	| Var
	| Num
	;

start syntax Expr2 
	= left Expr2 "+" Expr2
	| left Expr2 "*" Expr2
	| Var
	| Num
	;
lexical Var = [a-zA-Z] !<< [a-zA-Z]+ !>> [a-zA-Z];

lexical Num = [0-9]+;


layout LAYOUT = [\ \n\r\f\t]*;


data MyException = UnknownExpression(value v);

Expr addParens(Expr t) = bottom-up visit(t) {
	case (Expr)`<Num e>`: fail;
	case (Expr)`<Expr e>` => (Expr)`(<Expr e>)`
//	case x: println(x);
//	case (Expr)`<Expr e1> + <Expr e2>` => (Expr)`(<Expr e1> + <Expr e2>)`
//	case (Expr)`<Expr e1> * <Expr e2>` => (Expr)`(<Expr e1> * <Expr e2>)`
//	case (Expr)`<Expr e1> - <Expr e2>` => (Expr)`(<Expr e1> - <Expr e2>)`
//	case appl(prod(sort(/.*[Ee]xpr.*/), _, _), _): ;
};

Expr eval(Expr t) = bottom-up visit(t) {
	case (Expr)`<Num a> + <Num b>`: {
		int c = toInt("<a>") + toInt("<b>");
		insert parse(#Expr, "<c>");
	}
	case (Expr)`<Num a> - <Num b>`: {
		int c = toInt("<a>") - toInt("<b>");
		insert parse(#Expr, "<c>");
	}
	case (Expr)`<Num a> * <Num b>`: {
		int c = toInt("<a>") * toInt("<b>");
		insert parse(#Expr, "<c>");
	}
};



void compile(Expr t) {
	bottom-up visit(t) {
		case (Expr)`<Num n>`:
			println("push <n>");
		case (Expr)`<Var v>`:
			println("push <v>");
		case (Expr)`<Expr e1> + <Expr e1>`:
			println("add");
		case (Expr)`<Expr e1> * <Expr e2>`:
			println("mul");
		case (Expr)`<Expr e1> - <Expr e2>`:
			println("sub");
	};
}


bool parses(str s) = parse(#Expr, s)?;

str pretty(amb(alternatives)) {
	str s = "";
	for(alt <- alternatives) {
		s = s + "\n====\n<pretty(alt)>\n====\n";
	}
	return s;
}


str pretty((Expr)`(<Expr e>)`) {
	s = pretty(e);
	return "(" + s + ")";
}

str pretty((Expr)`<Expr e1> * <Expr e2>`) {
	s1 = pretty(e1);
	s2 = pretty(e2);
	return "(<s1> * <s2>)";
}

str pretty((Expr)`<Expr e1>+<Expr e2>`) {
	return "(<pretty(e1)> + <pretty(e2)>)";
}

str pretty((Expr)`<Expr e1> - <Expr e2>`) = "(<pretty(e1)> - <pretty(e2)>)";

str pretty((Expr)`<Expr e1><Expr e2>`) = "<pretty(e1)> <pretty(e2)>";

str pretty((Expr)`<Num a>`) = "<a>";

str pretty((Expr)`<Var a>`) = "<a>";

default str pretty(Expr e) {
	return unparse(e); // throw UnknownExpression(e);
}

//str pretty((Expr)`if <Expr c> then <Expr t> else <Expr e> end`) {
//	return "if <pretty(c)> then\n\t<pretty(t)>\nelse\n\t<pretty(e)>\nend\n";
//}

str prettycatch(Expr e) {
	try {
		return pretty(e);
	}
	catch UnknownExpression(x): return "[<x>]";
	catch /Unknown expression.*/: return "[" + unparse(e) + "]";
}

//	> left Expr "*" Expr
//	> left (Expr "+" Expr
//	     |  Expr "-" Expr)
//	| Var
//	| Num
