module JsonTest

import Json;
import ParseTree;
import String;
import IO;
import util::Math;
import List;

list[str] jsonIntegers = [
    "0", "1", "2", "3232892", "-43"
];


list[str] jsonNumbers = [
    *jsonIntegers,
    "21.321", "0.232", "0.45343e-21",
    "-43", "-0.232", "-43222.213e44"
];


list[str] jsonStrings = [
    "\"abc\"", "\"\"", "\"ab\\\"c\"",    
    "\"ab\\nc\"", "\"\\r\"", "\"a\\u1234b\\\"c\""
];

list[str] jsonBasicValues = [*jsonNumbers, *jsonStrings];

public str randomJson(int chance) {
    r = arbInt(chance);
    if(chance < 50) {
    switch(r) {
    case 0:
        return randomJsonList(chance+10);
    case 1:
        return randomJsonObject(chance+10);
    }
    }

    return getOneFrom(jsonIntegers);
}

str randomJsonList(int chance) {
    return "[<intercalate(",", [randomJson(chance) | _ <- [0 .. arbInt(10)]])>]";
}

str randomJsonObject(int chance) {
    return "{<intercalate(",", ["<getOneFrom(jsonStrings)> : <randomJson(chance)>" | _ <- [0 .. arbInt(10)]])>}";  // TODO: fill in, similar to randomJsonList
}

bool testList(list[str] ss) {
    r = true;
    for(s <- ss) {
        if(s != "") {
            try {
                parse(#JsonValue, s);
            }
            catch _: {
                println("Failed to parse: <s>");
                r = false;
            }
        }
    }
    return r;
}

test bool parseTestIntegers() {
    return testList(jsonIntegers);
}

test bool parseTestNumbers() {
    return testList(jsonNumbers);
}

test bool parseTestStrings() {
    return testList(jsonStrings);
}

test bool parseTestBasics() {
    return testList(jsonBasicValues);
}

test bool parseTestMore() {
    return testList([randomJson(5) | _ <- [0 .. 100]]);
}

bool toValueTestList(list[str] ss) {
    r = true;
    for(s <- ss) {
        if(s != "") {
            try {
                jsonToValue(parse(#JsonValue, s));
            }
            catch _: {
                println("Failed to parse: <s>");
                r = false;
            }
        }
    }
    return r;
}


test bool toValueTestIntegers() {
    return toValueTestList(jsonIntegers);
}

test bool toValueTestNumbers() {
    return toValueTestList(jsonNumbers);
}

test bool toValueTestStrings() {
    return toValueTestList(jsonStrings);
}

test bool toValueTestBasics() {
    return toValueTestList(jsonBasicValues);
}

test bool toValueTestMore() {
    return toValueTestList([randomJson(5) | _ <- [0 .. 100]]);
}



