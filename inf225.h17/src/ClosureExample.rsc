module ClosureExample
import IO;

public void main() {
	list[str] l = ["mango", "banan", "eple", "appelsin"];
	
	str badFruit = "appelsin";
	
	str fun(str s) {
		if(s == badFruit) {
			badFruit = "appelsin"; return "sitron";
		}
		else {
			return s;
		}
	}
	
	badFruit = "eple";
	
	println([fun(x) | x <- l]);	
}
