module Simpl
import ParseTree;
import IO;
import String;

start syntax Program
	= Def* Expr
	;
	
syntax Def 
	= Type Var "(" Type Var ")" "=" Expr ";"
	| Type Var "=" Expr ";"
	;
	
syntax Stat
	= IfThenElse: "if" Expr "then" Stat* "else" Stat* "end"
	| IfThen: "if" Expr "then" Stat* "end"
	| While: "while" Expr "do" Stat* "end"
	| Decl: Type Var "=" Expr ";"
	| left Assign:  Var "=" Expr ";"
	| Block: "{" Stat* "}"
	;
start syntax Expr 
	=  "(" Expr ")"
//	> "-" Expr
	> Apply: Expr "(" {Expr ","}* ")"
	> left Expr "*" Expr
	> left (Expr "+" Expr
	     |  Expr "-" Expr)
	> left Expr "\<" Expr
//	| Let: "let" Var "=" Expr "in" Expr "end"
//	| LetFun: "let" Type Var "(" Type Var ")" "=" Expr "in" Expr "end"
	| Lambda: "(" Var ")" "-\>" Expr
	| Var: Var name
	| Int: Num i
	;

syntax Type
	= "int"
	| Type "-\>" Type
	;
	
lexical Var = [a-zA-Z] !<< [a-zA-Z] [a-zA-Z0-9]* !>> [a-zA-Z];

lexical Num = [0-9]+;


layout LAYOUT = [\ \n\r\f\t]* !>> [\ \n\r\f\t];

anno str node@category;

data MyException = UnknownExpression(value v);
/*
Expr addParens(Expr t) = top-down visit(t) {
	case (Expr)`<Num e>`: fail;
	case (Expr)`<Expr e>` => (Expr)`(<Expr e>)`
	*/
//	case x: println(x);
//	case (Expr)`<Expr e1> + <Expr e2>` => (Expr)`(<Expr e1> + <Expr e2>)`
//	case (Expr)`<Expr e1> * <Expr e2>` => (Expr)`(<Expr e1> * <Expr e2>)`
//	case (Expr)`<Expr e1> - <Expr e2>` => (Expr)`(<Expr e1> - <Expr e2>)`
//	case appl(prod(sort(/.*[Ee]xpr.*/), _, _), _): ;
/*};

Expr partEval(Expr t) = bottom-up visit(t) {
	case (Expr)`<Num a> + <Num b>`: {
		int c = toInt("<a>") + toInt("<b>");
		insert parse(#Expr, "<c>");
	}
	case (Expr)`<Num a> - <Num b>`: {
		int c = toInt("<a>") - toInt("<b>");
		insert parse(#Expr, "<c>");
	}
	case (Expr)`<Num a> * <Num b>`: {
		int c = toInt("<a>") * toInt("<b>");
		insert parse(#Expr, "<c>");
	}
	case (Expr)`if <Num c> then <Expr th> else <Expr el> end`: {
		if(toInt("<c>") != 0) {
			insert th;
		}
		else {
			insert el;
		}
	}
};
*/
/*
Expr desugar(Expr e) {
	return visit(e) {
	case (Expr)`(<Var v>) -\> <Expr body>`
		=> (Expr)`let int FOO(int <Var v>) = <Expr body> in FOO end`
	}
}*/

bool parses(str s) = parse(#Expr, s)?;


